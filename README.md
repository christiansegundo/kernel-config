# README #

THESE FILES ARE PROVIDED AS-IS WITHOUT ANY WARRANTY, EXPRESSED NOR
IMPLIED.

**USE AT YOUR OWN RISK!**


### About ###

Here are some kernel configurations for various hardware I own.

I set my systems up per the recommended defaults of the Gentoo Handbook.
This implies virtual/logger, virtual/cron, ext4, OpenRC, GRUB 2... I usually
use gentoo-sources, but will note any variance in each config's notes.

Currently included:

 - Gigabyte Z77-D3H + i7-2600K + GTX1080 (nvidia-sources) workstation
 - MacBookPro8,1 (i5-2415M) laptop

### How to use ###

Clone this repo, then replace /usr/src/linux/.config with the appropriate kernel config file.

For example:

	git clone https://christiansegundo@bitbucket.org/christiansegundo/kernel-config.git
    cp ~/kernel-config/<hardware>/kernel-config-X.XX-gentoo /usr/src/linux/.config

Then build as you normally would. Don't forget to build/rebuild your initramfs and GPU drivers as applicable!

Kernel updating works well with the 'make oldconfig' command. It's not
likely I'll remember to keep these up to date all the time :)
